import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Hersteller 
{
	private String herstellername;
	private String ansprechperson;
	private String postleitzahl;
	private String strasse;
	private String hausnummer;
	private String query;

	public static Connection c;
	public static Statement stmt;

	public Hersteller (String hname, String person, String plz, String str, String hnr)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			stmt = c.createStatement();
			
			query = "CREATE TABLE IF NOT EXISTS `Muellentsorgung`.`Hersteller` (" + 
					"`HerstellerName` VARCHAR(45) NOT NULL," + 
					"`Ansprechperson` VARCHAR(45) NOT NULL," + 
					"`PLZ` TINYINT(4) UNSIGNED NOT NULL," +
					"`Strasse` VARCHAR(45) NOT NULL," +
					"`hausnummer` TINYINT(3) UNSIGNED NOT NULL," +
					"PRIMARY KEY (`HerstellerName`))" +
					"ENGINE = InnoDB;";
			
			stmt.executeUpdate(query);
			query = "";
			
			query = "INSERT INTO `Muellentsorgung`.`Hersteller` VALUES (" +
					hname + "," + 
					person + "," +
					plz + "," +
					str + "," +
					hnr + ");";
			
			stmt.executeUpdate(query);
			query = "";
			
			setAnsprechperson(person);
			setHerstellername(hname);
			setPostleitzahl(plz);
			setHausnummer(hnr);
			setStrasse(str);
		}
		catch (SQLException ex) 
		{
		    System.err.println("SQLException: " + ex.getMessage());
		    System.err.println("SQLState: " + ex.getSQLState());
		    System.err.println("VendorError: " + ex.getErrorCode());
		} 
		catch (Exception ex)
		{
			System.err.println(ex.getMessage());
		}

	}
	

	public String getHerstellername() 
	{
		return herstellername;
	}

	public void setHerstellername(String herstellername) 
	{
		this.herstellername = herstellername;
	}

	public String getAnsprechperson() 
	{
		return ansprechperson;
	}

	public void setAnsprechperson(String ansprechperson) 
	{
		this.ansprechperson = ansprechperson;
	}

	public String getPostleitzahl() 
	{
		return postleitzahl;
	}

	public void setPostleitzahl(String postleitzahl) 
	{
		this.postleitzahl = postleitzahl;
	}

	public String getStrasse() 
	{
		return strasse;
	}

	public void setStrasse(String strasse) 
	{
		this.strasse = strasse;
	}

	public String getHausnummer() 
	{
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) 
	{
		this.hausnummer = hausnummer;
	}

}