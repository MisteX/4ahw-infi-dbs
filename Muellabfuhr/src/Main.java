import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;

public class Main 
{
	public static Connection c;
	public static Statement stmt;
	public static BufferedReader brIn = new BufferedReader(new InputStreamReader(System.in));
	public static ResultSet rs;
	public static FileReader file;
	public static boolean command = false;
	public static ArrayList<String> table = new ArrayList<String>(); 
	
	public static void main(String[] args)
	{
		String username;
		String pw;
		BufferedReader lineText;
		String line;
		String tbl = "";
		
		try 
		{
			username = args[0];
			pw = args[1];
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			c = DriverManager.getConnection("jdbc:mysql://localhost?useSSL=false", username, pw);
			stmt = c.createStatement();
			
			stmt.execute("drop database Muellentsorgung");
			stmt.execute("create database Muellentsorgung");
			stmt.execute("use Muellentsorgung");
			
			System.out.println("Specify your File-Path:");
			readFile(brIn.readLine());
			
			lineText = new BufferedReader(file);
			line = lineText.readLine();
			
			while(line != null)
			{
				if (!command)
				{
					line = checkLine(lineText.readLine());
				}
				if(line.indexOf(";") >= 0)
				{
					command = false;
				}
//				System.out.println("Still running!");
				
				table.add(line);
				
				line = lineText.readLine();
			}
			
			for(int i = 0; i < table.size(); i++)
			{
				if(table.get(i).equals(""))
				{
					table.remove(i);
				}
				
				if(table.get(i).indexOf(";") < 0)
				{
					tbl += table.get(i);
				}
				else
				{
					tbl += table.get(i);
					stmt.executeUpdate(tbl);
				}
			}
			System.out.println("finished");
			
			
		}
		catch (SQLException ex) {
		    // handle any errors
		    System.err.println("SQLException: " + ex.getMessage());
		    System.err.println("SQLState: " + ex.getSQLState());
		    System.err.println("VendorError: " + ex.getErrorCode());
			
		} 
		catch (Exception e) 
		{
			System.err.println("Error: " + e.getMessage());
		} 
//			System.out.println(table.get(i));
	}
	
	public static void readFile (String path)
	{
		try 
		{
			file = new FileReader(path);
			System.out.println("File successfully read.");
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static String checkLine (String line)
	{
		line = line.toLowerCase();
		if(line.startsWith("create")||line.startsWith("insert"))
		{
			command = true;
			return line;
		}
		else if(line.startsWith("alter")||line.startsWith("update"))
		{
			command = true;
			return line;
		}
		else if(line.startsWith("select"))
		{
			command = true;
			return line;
		}
		
		line = "";
		return line;
	}
	
//	public static void createTable (String tblName, String args, String prKey) throws SQLException
//	{
//		String sql = "CREATE TABLE " + tblName + " ( " 
//				+ args + " ,PRIMARY KEY(" + prKey + "));";
//		
//		stmt.executeUpdate(sql);
//		stmt.close();
//	}
	
//	public static void insertTable (String tblName, String args, String values) throws SQLException
//	{
//		String sql = "INSERT INTO " + tblName + " ( " 
//				+ args + "VALUES (" + values +");";
//		
//		stmt.executeUpdate(sql);
//		stmt.close();
//	}
}