import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Muelllaster 
{
	private String kennzeichen;
	private String modellnummer;
	private String hersteller;

	private String query;

	public static Connection c;
	public static Statement stmt;

	public Muelllaster (String kennz, String hst, String mnr)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			stmt = c.createStatement();
			
			query = "CREATE TABLE IF NOT EXISTS `Muellentsorgung`.`Mülllaster` (" +
					"`Kennzeichen` VARCHAR(20) NOT NULL," +
					"`Modellnummer` SMALLINT(6) UNSIGNED NOT NULL," +
					"`Hersteller` VARCHAR(30) NOT NULL," +
					"`Hersteller_HerstellerName` VARCHAR(45) NOT NULL," +
					"PRIMARY KEY (`Kennzeichen`, `Hersteller_HerstellerName`))" +
					"ENGINE = InnoDB;" +

					"CREATE INDEX `fk_Mülllaster_Hersteller1_idx` ON `Muellentsorgung`.`Mülllaster` ("+
					"`Hersteller_HerstellerName` ASC);";

			stmt.executeUpdate(query);
			query = "";
			
			query = "INSERT INTO `Muellentsorgung`.`Muelltonne` VALUES (" +
					kennz + "," + 
					mnr + "," +
					hst + ");";
			
			stmt.executeUpdate(query);
			query = "";
			
			setKennzeichen(kennz);
			setHersteller(hst);
			setModellnummer(mnr);
			
		}
		catch (SQLException ex) 
		{
		    System.err.println("SQLException: " + ex.getMessage());
		    System.err.println("SQLState: " + ex.getSQLState());
		    System.err.println("VendorError: " + ex.getErrorCode());
		} 
		catch (Exception ex)
		{
			System.err.println(ex.getMessage());
		}

	}
	public String getKennzeichen() {
		return kennzeichen;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	public String getModellnummer() {
		return modellnummer;
	}

	public void setModellnummer(String modellnummer) {
		this.modellnummer = modellnummer;
	}

	public String getHersteller() {
		return hersteller;
	}

	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}
}
