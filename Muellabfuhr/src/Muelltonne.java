import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Muelltonne 
{
	private String modellname;
	private String hersteller;
	private String fassungsvermoegen;

	private String query;

	public static Connection c;
	public static Statement stmt;

	public Muelltonne (String mname, String hst, String fv)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			stmt = c.createStatement();
			
			query = "CREATE TABLE IF NOT EXISTS `Muellentsorgung`.`Muelltonne` (" +
					"`Modellname` VARCHAR(45) NOT NULL," +
					"`Hersteller` VARCHAR(45) NOT NULL," +
					"`Fassungsvermoegen` SMALLINT(4) UNSIGNED NOT NULL," +
					"`Hersteller_HerstellerName` VARCHAR(45) NOT NULL," +
					"PRIMARY KEY (`Modellname`, `Hersteller_HerstellerName`))" +
					"ENGINE = InnoDB;" +

					"CREATE INDEX `fk_M�lltonne_Hersteller1_idx`" +
					"ON `Muellentsorgung`.`M�lltonne` (`" +
					"Hersteller_HerstellerName` ASC);";


			
			stmt.executeUpdate(query);
			query = "";
			
			query = "INSERT INTO `Muellentsorgung`.`Muelltonne` VALUES (" +
					mname + "," + 
					hst + "," +
					fv + ");";
			
			stmt.executeUpdate(query);
			query = "";
			
			setFassungsvermoegen(fv);
			setHersteller(hst);
			setModellname(mname);
			
		}
		catch (SQLException ex) 
		{
		    System.err.println("SQLException: " + ex.getMessage());
		    System.err.println("SQLState: " + ex.getSQLState());
		    System.err.println("VendorError: " + ex.getErrorCode());
		} 
		catch (Exception ex)
		{
			System.err.println(ex.getMessage());
		}

	}
	public String getModellname() 
	{
		return modellname;
	}

	public void setModellname(String modellname) 
	{
		this.modellname = modellname;
	}

	public String getHersteller() 
	{
		return hersteller;
	}

	public void setHersteller(String hersteller) 
	{
		this.hersteller = hersteller;
	}

	public String getFassungsvermoegen() 
	{
		return fassungsvermoegen;
	}

	public void setFassungsvermoegen(String fassungsvermoegen) 
	{
		this.fassungsvermoegen = fassungsvermoegen;
	}
	
}
