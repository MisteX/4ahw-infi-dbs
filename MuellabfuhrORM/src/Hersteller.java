import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Hersteller 
{
//	@ForeignCollectionField
	@DatabaseField(id = true)
	private String herstellername;

	@DatabaseField(canBeNull = true)
	private String ansprechperson;
	
	@DatabaseField(canBeNull = true)
	private int postleitzahl;
	
	@DatabaseField(canBeNull = true)
	private String strasse;
	
	@DatabaseField(canBeNull = true)
	private int hausnummer;

	@ForeignCollectionField
	private Collection<Muelltonne> muelltonnen;
	
	@ForeignCollectionField
	private Collection<Muelllaster> muelllaster;
	
	public Hersteller() { } //Defaultkonstruktor f�r ORM
	
	public Hersteller (String herstellername, String ansprechperson, int postleitzahl, String strasse, int hausnummer)
	{
		setHerstellername(herstellername);
		setAnsprechperson(ansprechperson);
		setPostleitzahl(postleitzahl);
		setStrasse(strasse);
		setHausnummer(hausnummer);
	}
	
	public String getHerstellername() 
	{
		return herstellername;
	}

	public void setHerstellername(String herstellername) 
	{
		this.herstellername = herstellername;
	}

	public String getAnsprechperson() 
	{
		return ansprechperson;
	}

	public void setAnsprechperson(String ansprechperson) 
	{
		this.ansprechperson = ansprechperson;
	}

	public int getPostleitzahl() 
	{
		return postleitzahl;
	}

	public void setPostleitzahl(int postleitzahl) 
	{
		this.postleitzahl = postleitzahl;
	}

	public String getStrasse() 
	{
		return strasse;
	}

	public void setStrasse(String strasse) 
	{
		this.strasse = strasse;
	}

	public int getHausnummer() 
	{
		return hausnummer;
	}

	public void setHausnummer(int hausnummer) 
	{
		this.hausnummer = hausnummer;
	}
}