import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class Main 
{
	private Dao<Hersteller, Integer> herstellerDao;
	private Dao<Muelllaster, Integer> muelllasterDao;
	private Dao<Muelltonne, Integer> muelltonneDao;
	private Dao<Stadtteil, Integer> stadtteilDao;
	private Dao<MuelllasterMuelltonne, Integer> muelllasterMuelltonneDao;
	private Dao<MuelllasterStadtteil, Integer> muelllasterStadtteilDao;

	public static void main (String[] args) throws Exception 
	{
		new Main().doMain(args);
	}

	private void doMain(String[] args) throws Exception 
	{
		ConnectionSource connectionSource = null;
		try 
		{
			connectionSource = new JdbcConnectionSource("jdbc:mysql://localhost:3306/Muellabfuhr?useSSL=false","root","MeinTollesPW");

			setupDatabase(connectionSource);
			readWriteData();

			System.out.println("\n\nIt seems to have worked\n\n");
		} 
		finally 
		{
			if (connectionSource != null) 
			{
				connectionSource.close();
			}
		}

	}

	private void setupDatabase(ConnectionSource connectionSource) throws Exception 
	{
		herstellerDao = DaoManager.createDao(connectionSource, Hersteller.class);

		try 
		{
			TableUtils.createTable(connectionSource, Hersteller.class);
		}
		catch (SQLException e)
		{
			TableUtils.dropTable(connectionSource, Hersteller.class, false);
			TableUtils.createTable(connectionSource, Hersteller.class);
		}
		muelllasterDao = DaoManager.createDao(connectionSource, Muelllaster.class);

		try
		{
			TableUtils.createTable(connectionSource, Muelllaster.class);
		}
		catch (SQLException e)
		{
			TableUtils.dropTable(connectionSource, Muelllaster.class, false);
			TableUtils.createTable(connectionSource, Muelllaster.class);
		}
		muelltonneDao = DaoManager.createDao(connectionSource, Muelltonne.class);

		try 
		{
			TableUtils.createTable(connectionSource, Muelltonne.class);
		}
		catch (SQLException e)
		{
			TableUtils.dropTable(connectionSource, Muelltonne.class, false);
			TableUtils.createTable(connectionSource, Muelltonne.class);
		}
		stadtteilDao = DaoManager.createDao(connectionSource, Stadtteil.class);	

		try
		{
			TableUtils.createTable(connectionSource, Stadtteil.class);
		}
		catch (SQLException e)
		{
			TableUtils.dropTable(connectionSource, Stadtteil.class, false);
			TableUtils.createTable(connectionSource, Stadtteil.class);
		}
		muelllasterMuelltonneDao = DaoManager.createDao(connectionSource, MuelllasterMuelltonne.class);
		
		try
		{
			TableUtils.createTable(connectionSource, MuelllasterMuelltonne.class);
		}
		catch (SQLException e)
		{
			TableUtils.dropTable(connectionSource, MuelllasterMuelltonne.class, false);
			TableUtils.createTable(connectionSource, MuelllasterMuelltonne.class);
		}
		muelllasterStadtteilDao = DaoManager.createDao(connectionSource, MuelllasterStadtteil.class);
		
		try
		{
			TableUtils.createTable(connectionSource, MuelllasterStadtteil.class);
		}
		catch (SQLException e)
		{
			TableUtils.dropTable(connectionSource, MuelllasterStadtteil.class, false);
			TableUtils.createTable(connectionSource, MuelllasterStadtteil.class);
		}
	}

	private void readWriteData() throws Exception 
	{
		Hersteller scania = new Hersteller("Scania", "Mustermann", 68504, "Teststrasse", 34);
		herstellerDao.create(scania);

		Muelllaster h28 = new Muelllaster("IL-876AB", "H28", scania, 10000);
		muelllasterDao.create(h28);

		Muelltonne tonne1 = new Muelltonne("Scantonne1", scania, 8000);
		muelltonneDao.create(tonne1);

		Stadtteil thaur = new Stadtteil(6065, "Thaur");
		stadtteilDao.create(thaur);

		MuelllasterStadtteil muellstadt = new MuelllasterStadtteil(h28 , thaur, "12:00:00");
		muelllasterStadtteilDao.create(muellstadt);

		MuelllasterMuelltonne muelllasterTonne = new MuelllasterMuelltonne(tonne1 , h28);
		muelllasterMuelltonneDao.create(muelllasterTonne);
	}
}