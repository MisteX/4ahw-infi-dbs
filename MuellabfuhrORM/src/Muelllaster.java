import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Muelllaster 
{
//	@ForeignCollectionField
	@DatabaseField(id = true)
	private String kennzeichen;
	
	@DatabaseField(canBeNull = true)
	private String modellnummer;
	
	@DatabaseField(foreign = true)
	private Hersteller hersteller;
	
	@DatabaseField(canBeNull = true)
	private int maxLeervolumen;

	@ForeignCollectionField
	private Collection<MuelllasterMuelltonne> muelllasterTonnen;
	
	@ForeignCollectionField
	private Collection<MuelllasterStadtteil> muelllasterStadtteile;

	public Muelllaster() { } //Defaultkonstruktor f�r ORM
	
	public Muelllaster (String kennzeichen, String modellnummer, Hersteller hersteller, int maxleervolumen)
	{
		setHersteller(hersteller);
		setKennzeichen(kennzeichen);
		setModellnummer(modellnummer);
		setMaxLeervolumen(maxleervolumen);
	}
	
	public String getKennzeichen() 
	{
		return kennzeichen;
	}

	public void setKennzeichen(String kennzeichen) 
	{
		this.kennzeichen = kennzeichen;
	}

	public String getModellnummer() 
	{
		return modellnummer;
	}

	public void setModellnummer(String modellnummer) 
	{
		this.modellnummer = modellnummer;
	}

	public String getHersteller() 
	{
		return hersteller.getHerstellername();
	}

	public void setHersteller(Hersteller hersteller) 
	{
		this.hersteller = hersteller;
	}
	
	public int getMaxLeervolumen ()
	{
		return maxLeervolumen;
	}
	public void setMaxLeervolumen (int maxLeervolumen)
	{
		this.maxLeervolumen = maxLeervolumen;
	}
	
}
