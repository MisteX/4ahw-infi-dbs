import com.j256.ormlite.field.DatabaseField;

public class MuelllasterMuelltonne 
{
	@DatabaseField(foreign = true)
	private Muelltonne muelltonne;
	
	@DatabaseField(foreign = true)
	private Muelllaster muelllaster;
	
	public MuelllasterMuelltonne() { } //Default-Kontruktor f�r ORM
	
	public MuelllasterMuelltonne(Muelltonne muelltonne, Muelllaster muelllaster) 
	{ 
		setMuelllasterKennzeichen(muelllaster);
		setMuelltonneModellname(muelltonne);
	}

	public String getMuelltonneModellname() 
	{
		return muelltonne.getModellname();
	}

	public void setMuelltonneModellname(Muelltonne muelltonneModellname) 
	{
		this.muelltonne = muelltonneModellname;
	}

	public String getMuelllasterKennzeichen() 
	{
		return muelllaster.getKennzeichen();
	}

	public void setMuelllasterKennzeichen(Muelllaster muelllasterKennzeichen) 
	{
		this.muelllaster = muelllasterKennzeichen;
	}	
	
	
}
