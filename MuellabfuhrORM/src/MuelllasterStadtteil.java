import com.j256.ormlite.field.DatabaseField;

public class MuelllasterStadtteil 
{
	@DatabaseField(foreign = true)
	private Stadtteil stadtteil;
	
	@DatabaseField(foreign = true)
	private Muelllaster muelllaster;
	
	@DatabaseField(canBeNull = true)
	private String zeit;
	
	public MuelllasterStadtteil() { } //Default-Kontruktor f�r ORM
	
	public MuelllasterStadtteil(Muelllaster muelllaster, Stadtteil stadtteil,  String zeit) 
	{ 
		setMuelllasterKennzeichen(muelllaster);
		setStadtteilPostleitzahl(stadtteil);
		setZeit(zeit);
	}

	public int getStadtteilPostleitzahl() 
	{
		return stadtteil.getPostleitzahl();
	}

	public void setStadtteilPostleitzahl(Stadtteil stadtteil) 
	{
		this.stadtteil = stadtteil;
	}

	public String getMuelllasterKennzeichen() {
		return muelllaster.getKennzeichen();
	}

	public void setMuelllasterKennzeichen(Muelllaster muelllaster) 
	{
		this.muelllaster = muelllaster;
	}

	public String getZeit() {
		return zeit;
	}

	public void setZeit(String zeit) {
		this.zeit = zeit;
	}

	
}
