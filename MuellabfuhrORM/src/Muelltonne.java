import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Muelltonne 
{
//	@ForeignCollectionField
	@DatabaseField(id = true)
	private String modellname;
	
	@DatabaseField(foreign = true)
	private Hersteller hersteller;
	
	@DatabaseField(canBeNull = true)
	private int fassungsvermoegen;
	
	@ForeignCollectionField
	private Collection<MuelllasterMuelltonne> muelltonnenLaster;
	
	public Muelltonne() { } //Defaultkonstruktor f�r ORM
	
	public Muelltonne (String modellname, Hersteller hersteller, int fassungsvermoegen)
	{
		setHersteller(hersteller);
		setModellname(modellname);
		setFassungsvermoegen(fassungsvermoegen);
	}
	
	public String getModellname() 
	{
		return modellname;
	}

	public void setModellname(String modellname) 
	{
		this.modellname = modellname;
	}

	public String getHersteller() 
	{
		return hersteller.getHerstellername();
	}

	public void setHersteller(Hersteller hersteller) 
	{
		this.hersteller = hersteller;
	}

	public int getFassungsvermoegen() 
	{
		return fassungsvermoegen;
	}

	public void setFassungsvermoegen(int fassungsvermoegen) 
	{
		this.fassungsvermoegen = fassungsvermoegen;
	}
	
}
