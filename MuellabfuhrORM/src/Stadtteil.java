import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Stadtteil 
{
//	@ForeignCollectionField
	@DatabaseField(id = true)
	private int postleitzahl;
	
	@DatabaseField(canBeNull = true)
	private String bezeichnung;
	
	@ForeignCollectionField
	private Collection<MuelllasterStadtteil> stadtteilmuelllaster;

	public Stadtteil() { } //Defaultkonstruktor f�r ORM
	
	public Stadtteil (int postleitzahl, String bezeichnung)
	{
		setPostleitzahl(postleitzahl);
		setBezeichnung(bezeichnung);
	}

	public int getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(int postleitzahl) {
		this.postleitzahl = postleitzahl;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
}
